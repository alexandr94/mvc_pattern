<?php
require_once 'core/Route.php';
require_once 'core/Router.php';
require_once 'core/RouteMap.php';
require_once 'core/RoutingDispatcher.php';

$routeMap = new RouteMap();

$routeMap->draw([
    ['name' => 'index',  'pattern' => '/',                 'controller' => 'IndexController', 'action' => 'indexAction'],
    ['name' => 'users',  'pattern' => '/users',            'controller' => 'UsersController', 'action' => 'indexAction'],
    ['name' => 'user',   'pattern' => '/users/:id',        'controller' => 'UsersController', 'action' => 'showAction'],
    ['name' => 'user',   'pattern' => '/users/:id/:name',  'controller' => 'UsersController', 'action' => 'showIDName'],
    ['name' => 'about',  'pattern' => '/about',            'controller' => 'PagesController', 'action' => 'aboutAction'],
    ['name' => '404',    'pattern' => '/404',              'controller' => 'PagesController', 'action' => 'notFoundAction']
]);

RouteMap::$instance = $routeMap;

$dispatcher = new RoutingDispatcher();
$dispatcher->dispatch($_SERVER['REQUEST_URI']);


