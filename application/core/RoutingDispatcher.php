<?php

class RoutingDispatcher {

    private function extractParams(Route $route, $url) {
        //  /users/:id
        $regex =  preg_replace('/:[a-zA-z0-9]+\/?/', '', $route->getPattern());
        return preg_split('/\//', str_replace($regex, '', $url));
    }

    public function dispatch($url){
        $router = new Router();
        $routeMap = RouteMap::$instance;
        $route = new Route($router->match($routeMap, $url));
        var_dump($route);

        if (!$route){
            # TODO Handle 404
        } else {
            $controllerName = $route->getControllerName();
            $actionName = $route->getActionName();
            //create controller
            if (file_exists("application/controllers/$controllerName" . '.php')) {
                require_once("application/controllers/$controllerName" . '.php');
                if (class_exists($controllerName)) {
                    $controller = new $controllerName();
                    if (method_exists($controller, $actionName))
                        $controller->$actionName($this->extractParams($route, $url));
                }
            }
        }
    }
} 