<?php

/**
 * Class RouteMap contains Routes array and public static member $instance.
 * Router map gives Router to compare with REQUEST_URI.
 */
class RouteMap {
    private $_map;

    public function __construct(){
        $this->draw([]);
    }

    public function draw($map){
        $this->_map = $map;
    }

    public function get($name = null){
        if ($name == null)
            return $this->_map;
        else
            return isset($this->_map[$name]) ? $this->_map[$name] : null;
    }

    /**
     * @var RouteMap
     */
    public static $instance;
} 