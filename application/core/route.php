<?php
    class Route{
        private $_attributes;

        public function __construct($data){
            $this->_attributes = $data;
        }

        public function getControllerName(){
            return $this->_attributes['controller'];
        }

        public function getPattern() {
            return $this->_attributes['pattern'];
        }

        public function getActionName() {
            return $this->_attributes['action'];
        }

    }


