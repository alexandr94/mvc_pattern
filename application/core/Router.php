<?php

class Router {
    public function match(RouteMap $map, $url){
        foreach ($map->get() as $route) {
            $regex = preg_replace('/:[a-zA-z0-9]+/', '([a-zA-z0-9]+)', $route['pattern']);
            $regex = '/^' . preg_replace('/\//', '\/', $regex) . '\/?$/';
            if (preg_match($regex, $url)){
                return $route;
            }
        }
    }
}
